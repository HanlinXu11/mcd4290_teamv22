/* Write your function for implementing the Serial Search Algorithm here */
function serialSearchTest(array){
  // initialize index to zero 
  let index=0
  let endIndex= array.length
  while(index<endIndex){
    if(array[index].emergency){
      return array[index].address
    }
    index++
  }
  if(index==endIndex){
    return null
  }
}