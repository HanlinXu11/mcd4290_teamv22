// this page JS codes that share in all pages 






            class Trip
            {
                constructor(id)
                {
                    
                    this._id = id; // the id for trip is used to create trips (T1,T2)
                    this._DeparturePlace = ""; // Departure Place of Trip
                    this._ArrivalPlace=""; // Arrival Place 
                    this._DepartureAirport="";// Arrival Airport
                    this._ArrivalAirport=""
                    this._DepartureTime=0; 
                    this._UserName=""// name of passenger
                    this._stop=0//num of stop
                }
                
                
                //getter
                get id()
                {
                    return this._id;
                }
                get stop()
                {
                    return this._stop;
                }
                get DeparturePlace(){
                    return this._DeparturePlace;
                }
                get ArrivalPlace(){
                    return this._ArrivalPlace
                }
                get DepartureAirport(){
                    return this._DepartureAirport
                }
                get ArrivalAirport(){
                    return this._ArrivalAirport
                }
                get DepartureTime(){
                    return this._DepartureTime
                }
                get UserName(){
                    return this._UserName
                }
                
                
                //setter
                set stop(Newstop)
                {
                    this._stope=Newstop
                }
                set DeparturePlace(NewDeparturePlace)
                {
                    this._DeparturePlace=NewDeparturePlace
                }
                
                set UserName(NewUserName)
                {
                    this._UserName=NewUserName
                }
                
                set ArrivalPlace(NewArrivalPlace)
                {
                    this._ArrivalPlace=NewArrivalPlace
                }
                
                set DepartureAirport (NewDepartureAirport)
                {    
                    this._DepartureAirport=NewDepartureAirport
                }
                
                set ArrivalAirport(NewArrivalAirport)
                {
                    this._ArrivalAirport=NewArrivalAirport
                }
                
                set DepartureTime(NewDepartureTime)
                {
                    this._DepartureTime=NewDepartureTime
                }
                
                fromData(Trip)
                {
                    this._id = Trip._id;
                    this._DeparturePlace = Trip._DeparturePlace;   
                    this._ArrivalPlace =Trip._ArrivalPlace; 
                    this._DepartureAirport=Trip._DepartureAirport;
                    this._ArrivalAirport=Trip._ArrivalAirport;
                    this._DepartureTime=Trip._DepartureTime;
                    this._UserName= Trip._UserName;
                    
                }
                
                }
       

 
// TODO: Write code to implement the TripList class

            class TripList
                {
                    constructor()
                    {
                        this._TripListArray=[]
                    }
                    get Trips(){
                        return this._TripListArray
                    }
                    getTrip(index){
                        return this._TripListArray[index]
                    }
                    get count(){
                        return this._TripListArray.length
                    }
                    addTrips(id){
                        let newTrip= new Trip(id)
                        this._TripListArray.push(newTrip)
                    }
                    removeTrip(Id){
                        for( let i = 0; i < this._TripListArray.length; i++){ 
                            if ( this._TripListArray[i].id === Id){
                            this._TripListArray.splice(i, 1); 
                            }
                        }
                    }
                    fromdata(array){
                        
                        let data = array._TripListArray;
                        this._TripListArray = [];
                        for(let i = 0; i < data.length; i++)
                        {
                            let TRIP = new Trip();
                            TRIP.fromData(data[i]);
                            this._TripListArray.push(TRIP);
                        }
                    }
                }



            // some functions need for local storages

            function updateLocalStorage(data) // update into localstorage
                    {
                        localStorage.setItem('TRIPS_ARRAY_KEY', JSON.stringify(data))
                    }



            function checkIfDataExistsLocalStorage() // check if has previous trips in the localstorage
                {
                    let redata=getDataLocalStorage()
                    let check
                    if(redata)      // this check if the input is 0 , null, NaN undefine... 
                    {
                        check =true
                    }
                    else
                    {
                        check = false
                    }
                    return check
                }

            function getDataLocalStorage(){ // get the data from the local storage
                let DummyOBJ=localStorage.getItem("TRIPS_ARRAY_KEY")
                return JSON.parse(DummyOBJ)
                }


            // some code to run while the page load ==> 1. check if web have trips data before 2. Yes => get the data 


            // Global Triplist instance variable
            let Triplist = new TripList();
            let check=checkIfDataExistsLocalStorage()
            if(check==true)
            {
               let data= getDataLocalStorage()
               
               Triplist.fromdata(data)
            }

            // following of functions is for onlick buttoms in the header 
            function Home(){
                window.location='main_H.html'
            }
            
            function UserPage(){
                window.location='user_A.html'
            }
            
            function Setting(){
                window.location='Setting_T.html'
            }
            





