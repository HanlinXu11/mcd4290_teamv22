/*setting_T.js is the JS file which will run the functionality
of the Setting_T.html*/


/*-----------------------
  settings' functionality
-------------------------*/

//querySelector, for targeting the selected HTML tags.
let htmlElem = document.querySelector('html')
let body1Elem = document.querySelector('#changeBg')
let body2Elem = document.querySelector('#changeBg')

let h2Elem = document.querySelector('#a-set-font')
let divElem = document.querySelector('#other-div')
//etc.. can be any HTML tag

//getElementById
let aBgColor = document.getElementById('aBgColor');
let fontFam = document.getElementById('fontFam');
let fontSize = document.getElementById('sizes');
let fontColor = document.getElementById('colors');

//
if(!localStorage.getItem('aBgColor')) {
  populateStorage();
} else {
  setStyles();
}

//
function populateStorage() {
    // Font Types (1)
    localStorage.setItem('aBgColor', document.getElementById('aBgColor').value);
    // Change Font Sizes (2)
    localStorage.setItem('fontFam', document.getElementById('fontFam').value);
    // Change Font Colors (3)
    localStorage.setItem('sizes', document.getElementById('sizes').value);
    // Change Theme Colors, either Black/White or Dark/Bright (4)
    localStorage.setItem('colors', document.getElementById('colors').value);
    //call setStyles function
    setStyles();
}

//
function setStyles() {
    //set variable, that get item from localStorage
    let currentColor = localStorage.getItem('aBgColor');
    let currentFontFam = localStorage.getItem('fontFam');
    let currentFontSize = localStorage.getItem('sizes');
    let currentFontColor = localStorage.getItem('colors');
    
    //Set the the value from id equals to a variable
    document.getElementById('aBgColor').value = currentColor;
    document.getElementById('fontFam').value = currentFontFam;
    document.getElementById('sizes').value = currentFontSize;
    document.getElementById('colors').value = currentFontColor;

    //Set the style of each color, font, size and background color
    body1Elem.style.backgroundColor = currentColor;
    h2Elem.style.fontFamily = currentFontFam;
    h2Elem.style.fontSize = currentFontSize;
    h2Elem.style.color = currentFontColor;
    
    
}

    //Change css/style and save to local storage
    aBgColor.onchange = populateStorage;
    fontFam.onchange = populateStorage;
    fontSize.onchange = populateStorage;
    fontColor.onchange = populateStorage;


// Reset Settings (5)
//Reset Font Family / Types
function resetA() {
  localStorage.removeItem('fontFam');
  //Reload
  location.reload();
}

//Reset Font Size
function resetB() {
  localStorage.removeItem('sizes');
  //Reload
  location.reload();
}

//Reset Font Color
function resetC() {
  localStorage.removeItem('colors');
  //Reload
  location.reload();
}

//Reset Background Color
function resetD() {
  localStorage.removeItem('aBgColor');
  //Reload
  location.reload();
}

//Reset All Settings
function resetE() {
  localStorage.clear();
  //Reload
  location.reload();
}


// Back to home page (6)
function backToHomePage() {
    //Redirecting to main page
    window.location="main_H.html"
}

