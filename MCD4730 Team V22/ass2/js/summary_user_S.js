var delete_span = document.getElementById("delete_span")  //Get the delete button under the map by id

// Get all the input input boxes on html get by tag name (btns get an element set)
var btns = document.getElementsByTagName("input")
// Add a click event to the delete button
delete_span.onclick = function () {

    // Loop the element set of btns (that is, loop all input tags)
    for (var i = 0; i < btns.length; i++) {
        // Make the value of each input element empty (delete)
        btns[i].value = ''
    }

}

// back home
var back = document.getElementById("back_home")
back.onclick = function () {
    // write a address here
    window.location.href = "./main_H.html"
}

