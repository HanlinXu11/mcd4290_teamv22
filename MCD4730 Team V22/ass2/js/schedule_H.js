let map
let markerArray=[]

            if('geolocation' in navigator) { //check if the geo is on
              navigator.geolocation.getCurrentPosition(ON,OFF)
            } 
            else {
              OFF()
            }

            function OFF(){
            alert("Geo is not ON")
            }

            function ON(location){
            // generate a map into the page
            mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
            map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    zoom: 5,
                    center: [location.coords.longitude,location.coords.latitude]
                })
        }




        // to fill all informations into countries's droplist
            let output=""

            for(let i=0;i<countryData.length;i++){
                output+="<option value='"+ countryData[i] + "'>"

            }

            document.getElementById("countries").innerHTML=output

            //this function runs after user click on apply buttom
            function ApplyOnMap(){ 
            let country= document.getElementById('DeparturePlace').value
            let Index= countryData.find(x => x ===country)
            
            
            
            if(typeof(Index)==="string")//the value of input is not valid
               {
                   // add into page by function 
                   document.getElementById("error").style.visibility = "hidden"
                   AirportPopup(country)
                   
               } // add into page HTML 

            else {document.getElementById("error").style.visibility = "visible";} 
        }


        // API and Map Functions
            function AirportPopup(country){
            let url='https://eng1003.monash/OpenFlights/airports/'
            let data={
            country:country,
            callback:'AirportCallBack'
        }
            webServiceRequest(url,data)
        }


        
            function clearHTML(ID){
            document.getElementById(ID).innerHTML=""
        }
        
            function removeMarkers(array){
            for(let i=0;i<array.length;i++){
                array[i].remove()
            }
            array=[]
            return array
        }


let lat=[]
let long=[]
let cities =[]
let airportID=[]
let AirportResult=[]
let AirportName=[]


            function AirportCallBack(result){
            lat=[]
            long=[]
            cities =[]
            airportID=[]
            AirportResult=[]
            console.log(result)
            for(let i=0;i<result.length;i++){
                lat.push(result[i].latitude)
                long.push(result[i].longitude)
                airportID.push(result[i].airportId)
                cities.push(result[i].city)
                AirportName.push(result[i].name)
                AirportResult.push(result[i])
            }
            
            // return all Cities into the datalist  
            document.getElementById("Cities").innerHTML=""
            let Output=""
            removeMarkers(markerArray);
            for(let i=0;i<cities.length;i++){
                Output+="<option value='"+ AirportName[i] + "'>"
                
            }
            document.getElementById("Cities").innerHTML=Output
                        
            map.setZoom(3);
            map.panTo([long[0],lat[0]])
            
            for(let i=0;i<lat.length;i++){
                markerArray.push(marker = new mapboxgl.Marker({
                color: "#FFFFFF",
                draggable: false
                }).setLngLat([long[i], lat[i]])
                .addTo(map))
            }
            
        }

let DepartureCity
let SelectedLocation=[]
let Arr_Cities
let Arr_SelectedLocation 
let Depar_airportID=[]

            function ShowTheDepartureAirport(){
            
            
            DepartureCity= document.getElementById('DepartureCities').value
            SelectedLocation = AirportResult.filter(x => x.name ===DepartureCity)
            if(SelectedLocation)//the value of input is not valid
               {
                   // add into page by function 
                   document.getElementById("error1").style.visibility = "hidden"
                    // airports IDs of departure
                
            for(let i=0;i<SelectedLocation.length;i++){
                Depar_airportID.push(SelectedLocation[i].airportId)
            }
            removeMarkers(markerArray);
                   removeLayerWithId('route')
            map.panTo([SelectedLocation[0].longitude, SelectedLocation[0].latitude])
            
            for(let i=0;i<SelectedLocation.length;i++){
            markerArray.push(marker = new mapboxgl.Marker({
            color: "red",
            draggable: false
            }).setLngLat([SelectedLocation[i].longitude, SelectedLocation[i].latitude])
            .addTo(map))
            }
            GetResultFromUrl(Depar_airportID)
            //DisplayOnDataList(route1,route2)
            return Depar_airportID
            
            
            // call the function to display the Cities in the Datalist
                   
                   
               } // add into page HTML 

            else {document.getElementById("error1").style.visibility = "visible";}
            
            
            
            
            
        }

            function GetResultFromUrl(sourceID){
            
                
            for(let i=0;i<sourceID.length;i++){
                let url1='https://eng1003.monash/OpenFlights/routes/'
                let data1={
                    sourceAirport:sourceID[i],
                    callback:'PrintResult'
                }
                webServiceRequest(url1,data1)   
            }// returns the array contains route info from the source AirportID
            
            
            let country= document.getElementById('DeparturePlace').value
            let url2='https://eng1003.monash/OpenFlights/allroutes/'
            let data2={
                country:country,
                callback:'PrintResult2'
            }
            webServiceRequest(url2,data2)
        }


let route1=[]
            function PrintResult(result){
            for(let i=0;i<result.length;i++){
                route1.push(result[i])
            }
        }
        
let route2=[]
        
            function PrintResult2(result){
            for(let i=0;i<result.length;i++){
                route2.push(result[i])
            }
            DisplayOnDataList(route1,route2)
        }

let ROUTE=[]
let DestinationAirports=[]
let DestinationAirportID=[]
let latitude=[]
let longtitude=[]
let arrivailCities =[]
        
            function DisplayOnDataList(route1,route2){
            ROUTE=[]
            let destinations=[]
            let AirportDetail=[]
            arrivailCities =[]
            for(let i=0;i<route1.length;i++){
                let Route1AirportId=JSON.stringify(route1[i].destinationAirportId)
                destinations.push(Route1AirportId)
            }
            for (let i=0;i<destinations.length;i++){
                let x=AirportResult.find(x => x.airportId ===destinations[i])
                if(x){
                    latitude.push(x.latitude)
                    longtitude.push(x.longitude)
                    arrivailCities.push(x.name)
                    DestinationAirports.push(x)
                    ROUTE.push(route1[i])
                    DestinationAirportID.push(route1[i].destinationAirportId)

                }
                
                
            }
                if(latitude.length==0){
                    alert('No Trip Is Found')
                }
            console.log(latitude)
            console.log(longtitude)
            // display on map first 
            for(let i=0;i<latitude.length;i++){
                markerArray.push(marker = new mapboxgl.Marker({
                color: "blue",
                draggable: false
                }).setLngLat([longtitude[i],latitude[i]])
                .addTo(map))
                
                
            }
            // display cities on datalist 
            document.getElementById("Arrival_City").innerHTML="" // clear the value first
            let OutPut=""
    
            for(let i=0;i<arrivailCities.length;i++){
                OutPut+="<option value='"+ arrivailCities[i] + "'>"
                
            }
            document.getElementById("Arrival_City").innerHTML=OutPut
          
        }
        

let chosenFinalCity
let FinalSelectedRoute
let finalSource
let FinalDesID
let finalDesAirPort
            function DisplayFinalRoute(){

                chosenFinalCity=document.getElementById("ArrivalCities").value

                if(arrivailCities.find(x=>x===chosenFinalCity))//the value of input is not valid
                   {
                        //show path
                        
                        let finalLat = latitude[arrivailCities.findIndex(x=> x===chosenFinalCity)]
                        let finalLong= longtitude[arrivailCities.findIndex(x=> x===chosenFinalCity)]
                                    
                        
                        FinalSelectedRoute = ROUTE[arrivailCities.findIndex(x=> x===chosenFinalCity)]
                        FinalDesID= DestinationAirportID[arrivailCities.findIndex(x=> x===chosenFinalCity)]
                        finalDesAirPort=DestinationAirports[arrivailCities.findIndex(x=> x===chosenFinalCity)]
                        let sourceAirportID = FinalSelectedRoute.sourceAirportId
                        finalSource= SelectedLocation.find(x=>x.airportId===JSON.stringify(sourceAirportID))
                        let finalSourceLat=finalSource.latitude
                        let finalSourceLong=finalSource.longitude
                        let SourceLoc=[finalSource.longitude,finalSource.latitude]
                        let FinalLoc=[finalLong,finalLat]
                        removeMarkers(markerArray);
                        markerArray.push(marker = new mapboxgl.Marker({
                            color: "red",
                            draggable: false
                            }).setLngLat(SourceLoc)
                            .addTo(map))

                        markerArray.push(marker = new mapboxgl.Marker({
                            color: "blue",
                            draggable: false
                            }).setLngLat(FinalLoc)
                            .addTo(map))
                        
                        
                        
                        removeLayerWithId('route')
                        
                        map.addSource('route', {
                        'type': 'geojson',
                        'data': {
                        'type': 'Feature',
                        'properties': {},
                        'geometry': {
                        'type': 'LineString',
                        'coordinates': [SourceLoc,FinalLoc]
                        }
                        }});
                        map.addLayer({
                        'id': 'route',
                        'type': 'line',
                        'source': 'route',
                        'layout': {
                        'line-join': 'round',
                        'line-cap': 'round'
                        },
                        'paint': {
                        'line-color': '#888',
                        'line-width': 8
                        }
                        })
                       // add into page by function 
                       document.getElementById("error2").style.visibility = "hidden"

                   } // add into page HTML 

                else {document.getElementById("error2").style.visibility = "visible"}



                //show path
            }
            

            function removeLayerWithId(idToRemove)
                {
                    let hasPoly = map.getLayer(idToRemove)
                    if (hasPoly !== undefined)
                    {
                        map.removeLayer(idToRemove)
                        map.removeSource(idToRemove)
                    }
                }
                     
            
            // this function runs when user want to check the name of airports
            function checkTheAirportName(){
                document.getElementById("error5").style.visibility = "hidden"
                let City = document.getElementById("CheckAirportName_City").value
                let country = document.getElementById("CheckAirportName_Country").value
                if(country&&City){
                    let url='https://eng1003.monash/OpenFlights/airports/'
                    let data={
                        country:country,
                        callback:'DisplayAllAirportsName'
                    }
                    webServiceRequest(url,data)
                }

            }

            
            function DisplayAllAirportsName(result){
                document.getElementById('target').innerHTML=""
                let City = document.getElementById("CheckAirportName_City").value
                let airportNames=[]
                
                let check=result.filter(x=>x.city===City)
                let Names= []
                if(check[0]){
                    for(let i=0;i<check.length;i++){
                        Names.push(check[i].name)
                    }
                    
                }
                
                else {document.getElementById("error5").style.visibility = "visible";}
                let board = document.getElementById('target');
                board.innerHTML = JSON.stringify(Names)
            }
            
            // following codes are used to update info into LocalStorage

            // this function calls when the user press submit Button 
            
            function UpdateToGlobalVariable(){
                document.getElementById("error4").style.visibility = "hidden"
                let InputName=document.getElementById('Name').value
                document.getElementById("error4").style.visibility = "hidden"
                let Date=document.getElementById('Date').value
                
                if(InputName){
                    if(Date){
                        let id= FinalSelectedRoute.airline+ FinalSelectedRoute.airlineId
                        Triplist.addTrips(id)
                        Triplist.Trips
                        let index=Triplist.count-1
                        let trip= Triplist.getTrip(index)
                        trip.UserName=InputName
                        trip.DeparturePlace=finalSource.city
                        trip.ArrivalPlace=finalDesAirPort.city
                        trip.DepartureAirport=finalSource.name
                        trip.ArrivalAirport=finalDesAirPort.name
                        trip.DepartureTime=Date
                        trip.stop=FinalSelectedRoute.stop
                        Triplist.Trips[index]= trip
                        updateLocalStorage(Triplist)
                        window.location='schedule_summary_S.html'
                    }
                    
                    else {document.getElementById("error3").style.visibility = "visible";
                          alert("missing Information")
                         }
                }
                else{document.getElementById("error4").style.visibility = "visible";
                    alert("missing Information")
                    }
            }
          
        

function datecheck() {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;    
   
  return maxDate
}
let x = datecheck()
document.getElementById("Date").min=x




        



