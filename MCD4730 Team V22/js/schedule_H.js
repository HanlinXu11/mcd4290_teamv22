let map


if('geolocation' in navigator) { //check if the geo is on
  navigator.geolocation.getCurrentPosition(ON,OFF)
} else {
  OFF()
}

function OFF(){
    alert("Geo is not ON")
}

function ON(location){
    // generate a map into the page
    mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
    map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 15,
            center: [location.coords.longitude,location.coords.latitude]
        })
}