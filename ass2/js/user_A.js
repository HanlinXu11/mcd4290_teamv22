function displaySchedule() {
    // Order: future, now, history
    let outputs = [];
    let seperated = seperate_trips();

    for (const list of seperated) {
        let output = "";
        for (let i = list.length-1; i >= 0; i--) {
            let [trip, index] = list[i];
            output += `
    <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand">
                <h4>
                Date: ${trip.DepartureTime}<br>
                Airline Number: ${trip.id}<br>
                Departure Airport: ${trip.DepartureAirport}<br>
                Arrival Airport: ${trip.ArrivalAirport}<br>
                </h4>
            </div>
            <div class="mdl-card__actions mdl-card--border">
                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="toScheduleDetails(${index})">
                    View Details
                </a>
                <a id="delete-button-${index}" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="deleteSchedule('${trip.id}')">
                    Delete
                </a>
            </div>
        </div>
    </div>
    `
        }
        outputs.push(output);
    }


    document.getElementById("FutureBriefSummary").innerHTML = outputs[0];
    document.getElementById("TodayBriefSummary").innerHTML = outputs[1];
    document.getElementById("HistoryBriefSummary").innerHTML = outputs[2];
    checkDate();
}

function toScheduleDetails(i) {
    localStorage.setItem("TRIPS_ARRAY_INDEX", i.toString());
    window.location = "user_summary_S.html";
}

function deleteSchedule(id) {
    if (confirm("Warning: Do you want to delete this schedule? All details in this schedule will be deleted.")) {
        Triplist.removeTrip(id);
        updateLocalStorage(Triplist);
        alert("This schedule has been deleted.");
        window.location = "user_A.html"
    }
}

function checkDate(){
    let future = get_future();
    for (let i = data.length-1; i >= 0; i--) {
        if (parse_date(data[i]) < future) {
            document.getElementById(`delete-button-${i}`).style.visibility = "hidden";
        }
    }
}


function get_future() {
    let future = new Date();
    future.setHours(0,0,0,0);
    future.setDate(future.getDate() + 2);
    return future;
}

function parse_date(trip) {
    let day = trip.DepartureTime.substring(8,10);
    let month = trip.DepartureTime.substring(5,7);
    let year = trip.DepartureTime.substring(0,4);

    return new Date(Number(year), Number(month)-1, Number(day));
}

function seperate_trips() {
    let future_trips = [];
    let commerced_trips = [];
    let history_trips = [];

    let future = get_future();
    let today = new Date();
    today.setHours(0,0,0,0);

    for (let i = 0; i < data.length; i++) {
        if (parse_date(data[i]) < today) {
            history_trips.push([data[i], i]);
        } else if (parse_date(data[i]) < future) {
            commerced_trips.push([data[i], i]);
        } else {
            future_trips.push([data[i], i]);
        }
    }

    return [future_trips, commerced_trips, history_trips];
}

let data = Triplist.Trips;
displaySchedule();