// codes runs while the page load

        function GetFromlocalStorage(){
            let index=JSON.parse(localStorage.getItem("TRIPS_ARRAY_INDEX"))
            let theTrip=JSON.parse(localStorage.getItem("TRIPS_ARRAY_KEY"))._TripListArray[index]
            let username=theTrip._UserName
            let ArrivalAirport=theTrip._ArrivalAirport
            let departureAirport=theTrip._DepartureAirport
            let arrivailCity=theTrip._ArrivalPlace
            let departureCity=theTrip._DeparturePlace
            let airline=theTrip._id
            let depTime=theTrip._DepartureTime
            let stop=theTrip._stop
                
            document.getElementById("Departure_Airport").innerHTML=departureAirport
            document.getElementById("LandingAirport").innerHTML=ArrivalAirport
            document.getElementById("DepartureTime").innerHTML=depTime
            document.getElementById("NumOfStop").innerHTML=stop
            document.getElementById("FlightNumber").innerHTML=airline
            document.getElementById("DepCity").innerHTML=departureCity
            document.getElementById("ArrCity").innerHTML=arrivailCity
            document.getElementById("Name").innerHTML=username
            console.log(theTrip)
            let arrivallat= theTrip.ArrivalLat
            let arrivallong = theTrip.ArrivalLong
            let departurelat= theTrip.DeparLat
            let departurelong = theTrip.DeparLong
            console.log(arrivallat)
            mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
            let map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    zoom: 3,
                    center: [arrivallong,arrivallat]
                })
            
            let marker1 = new mapboxgl.Marker({
                color: "red",
                draggable: false
                }).setLngLat([arrivallong, arrivallat])
                .addTo(map)
            let marker2 = new mapboxgl.Marker({
                color: "blue",
                draggable: false
                }).setLngLat([departurelong, departurelat])
                .addTo(map)
            
            setTimeout(function(){ map.addSource('route', {
            'type': 'geojson',
            'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
            'type': 'LineString',
            'coordinates': [[departurelong, departurelat],[arrivallong, arrivallat]]
            }
            }});
            map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
            'line-join': 'round',
            'line-cap': 'round'
            },
            'paint': {
            'line-color': '#888',
            'line-width': 8
            }
            }) }, 3000);
            
        }
        function LoadDetails(){
            setTimeout(GetFromlocalStorage(), 3000)
        }
LoadDetails()






        function Confirm(){
            window.location="main_H.html"
        }
        
        function Delete(){
            //
        }